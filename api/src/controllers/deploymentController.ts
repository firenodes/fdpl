import config from "../lib/config.js";
import k8s from "@kubernetes/client-node";
import { logger as mainLogger } from "../lib/logger.js";
import { Request, Response } from "express";
import User from "../models/user.js";

const kapi = config.kubeConfig.makeApiClient(k8s.AppsV1Api);

const logger = mainLogger.getChildLogger({
    name: "fdpl deployments"
});

export const listDeployments = async (req: Request, res: Response) => {
    const user = await User.findOne({
        _id: res.locals.jwt._id
    }).exec();

    if (!user) {
        return res.status(404).send({
            message: "User not found"
        });
    }

    const deployments = await kapi.listNamespacedDeployment("default");

    return res.status(200).send({
        deployments: deployments.body.items.filter(
            (deployment) => deployment.metadata?.labels?.fdpl_user === user._id
        )
    });
};
