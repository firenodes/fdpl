# @flowtr/fdpl

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)
[![Linting By ESLint](https://raw.githubusercontent.com/aleen42/badges/master/src/eslint.svg)](https://eslint.org)
[![Typescript](https://raw.githubusercontent.com/aleen42/badges/master/src/typescript.svg)](https://typescriptlang.org)

## Deploying in Production

### Building the Docker Images

```bash
cd k8s
chmod +x ./build.sh
./build.sh
```

### Applying the Kubernetes Configuration

```bash
cd k8s
# Create the mongo database
kubectl apply -f mongo/mongodb.yaml
# Run the backend server
kubectl apply -f api/api.yaml
# Run the caddy server that exposes an admin api to the backend server
kubectl apply -f caddy/proxy.yaml
```

## To Do

- Dynamic js/ts feature importing system
