import { useStore } from "nanostores/preact";
import { currentUser, setUser } from "../stores/user";
import { lang } from "../../../src/util/lang";

export const HomePage = () => {
    const user = useStore(currentUser);

    return user ? (
        <>
            <h2>{lang.t("welcome")(currentUser.value.username)}</h2>

            <button
                onClick={() => {
                    setUser(undefined);
                    window.localStorage.removeItem("user");
                    // reload page
                    window.location.reload();
                }}
            >
                {lang.t("logout")}
            </button>
        </>
    ) : (
        <p>
            <a href="/client/login">{lang.t("login")}</a>
        </p>
    );
};
