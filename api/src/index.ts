import cookieParser from "cookie-parser";
import cors from "cors";
import express from "express";
import mongoose from "mongoose";
import config from "./lib/config.js";
import { logger } from "./lib/logger.js";
import deploymentRouter from "./routes/deploymentRouter.js";
import userRouter from "./routes/userRouter.js";

try {
    await mongoose.connect(config.mongoUri, config.mongo);
    logger.info("Connected to mongoDB.");
} catch (error) {
    logger.prettyError(error as Error);
}

const app = express();

app.use((req, res, next) => {
    logger.info(
        `Method: [${req.method}] - URL: [${req.url}] - IP: [${req.socket.remoteAddress}]`
    );
    res.on("finish", () => {
        /** Log the response */
        logger.info(
            `METHOD: [${req.method}] - URL: [${req.url}] - STATUS: [${res.statusCode}] - IP: [${req.socket.remoteAddress}]`
        );
    });

    next();
});
app.use(express.json());
app.use(
    cors({
        credentials: true,
        origin: process.env.CLIENT_URI
    })
);
app.use(cookieParser());
app.use((req, res, next) => {
    next();
});

app.get("/", (req, res) => res.json({ message: "fdpl api is healty" }));
app.use("/users", await userRouter());
app.use("/deployments", await deploymentRouter());

// start express app
const port = process.env.PORT ?? 8080;
app.listen(port, () => {
    logger.info(`Listening on :${port}`);
});
