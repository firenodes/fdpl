import dotenv from "dotenv";
import { ConnectOptions } from "mongoose";
import k8s from "@kubernetes/client-node";
import os from "os";
import path from "path";

dotenv.config();

const mongoUri = process.env.MONGO_URI || "mongodb://localhost:27017/fdpl";

const mongoOptions: ConnectOptions = {
    maxPoolSize: 50
};

// caddy reverse proxy admin api
const caddyUri = process.env.CADDY_URI || "http://localhost:2019";

// jwt
const tokenOptions = {
    expireTime: process.env.TOKEN_EXPIRE_TIME || 3600,
    issuer: process.env.TOKEN_ISSUER || "coolissuer",
    secret: process.env.TOKEN_SECRET || "superencryptedsecret"
};

const kubeConfig = new k8s.KubeConfig();
kubeConfig.loadFromDefault();

const config = {
    kubeConfig,
    mongo: mongoOptions,
    mongoUri,
    token: tokenOptions,
    caddyUri
};

export default config;
