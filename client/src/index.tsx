import { HomePage } from "./pages/home";
import "./style/index.scss";
import { ToastContainer } from "react-toastify";
import { useStore } from "nanostores/preact";
import { router } from "./stores/router";
import { LoginPage } from "./pages/login";

export const Pages = () => {
    const page = useStore(router);
    if (page === undefined || page.route === undefined) {
        return (
            <>
                <h2>404</h2>
                <p>404 Not Found</p>
            </>
        );
    } else if (page.route === "home") return <HomePage />;
    else if (page.route === "login") return <LoginPage />;
    else
        return (
            <>
                <h2>404</h2>
                <p>404 Not Found</p>
            </>
        );
};

export const App = () => (
    <div id="container" className="app">
        <nav className="navbar">
            <h1 className="navbar-brand">
                <a href="https://projects.theoparis.com/fdpl">Flowtr Panel</a>
            </h1>
            <div className="navbar-list">
                <a href="/client">Home</a>
                <a href="/client/login">Login</a>
            </div>
        </nav>
        <ToastContainer />
        <main className="content">
            <Pages />
        </main>
    </div>
);
