import { Request, Response, NextFunction } from "express";
import { logger as mainLogger } from "../lib/logger.js";
import jwt from "jsonwebtoken";
import config from "../lib/config.js";

const logger = mainLogger.getChildLogger({
    name: "auth"
});

const extractJwt = (req: Request, res: Response, next: NextFunction) => {
    logger.debug("Validating token");

    const token = req.headers.authorization?.split(" ")[1];

    if (token) {
        jwt.verify(token, config.token.secret, (error, decoded) => {
            if (error) {
                return res.status(401).json({
                    message: error.message,
                    error
                });
            } else {
                res.locals.jwt = decoded;
                next();
            }
        });
    } else {
        return res.status(401).json({
            message: "No token provided"
        });
    }
};

export default extractJwt;
