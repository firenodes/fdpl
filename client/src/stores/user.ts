import { createStore, update } from "nanostores";
import { IUser } from "../../util/validation";

export const currentUser = createStore<IUser>(() => {
    currentUser.set(undefined);
});

currentUser.subscribe((user) => {
    if (user) {
        localStorage.setItem("user", JSON.stringify(user));
    } else if (localStorage.getItem("user")) {
        currentUser.set(JSON.parse(localStorage.getItem("user")));
    } else {
        localStorage.removeItem("user");
    }
});

export function setUser(user: IUser) {
    update(currentUser, () => user);
}
