import { IUser } from "@shared/index.js";
import mongoose from "mongoose";

const userSchema = new mongoose.Schema<IUser>(
    {
        username: { type: String, required: true },
        password: { type: String, required: true }
    },
    { timestamps: true }
);

const User = mongoose.model<IUser>("User", userSchema);

export default User;
