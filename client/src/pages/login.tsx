import { LoginForm } from "../components/login-form";

export const LoginPage = () => (
    <>
        <h2>Login</h2>
        <LoginForm />
    </>
);
