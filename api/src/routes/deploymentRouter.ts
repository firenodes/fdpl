import express from "express";
import * as controller from "../controllers/deploymentController.js";
import extractJwt from "../middleware/extractJwt.js";

const deploymentRouter = async () => {
    const router = express.Router();

    router.get("/list", extractJwt, controller.listDeployments);

    // router.get

    return router;
};

export default deploymentRouter;
