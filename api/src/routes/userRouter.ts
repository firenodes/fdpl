import express from "express";
import * as controller from "../controllers/userController.js";
import extractJwt from "../middleware/extractJwt.js";

const authRouter = async () => {
    const router = express.Router();

    router.get("/profile", extractJwt, controller.getProfile);
    router.post("/register", controller.register);
    router.post("/login", controller.login);

    // router.get

    return router;
};

export default authRouter;
