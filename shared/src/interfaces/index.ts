import type { Document } from "mongoose";

export interface IUser extends Document {
    username: string;
    password: string;
}

export interface ICaddyRoute {
    handle: (
        | {
              handler: "static_response";
              body: string;
          }
        | {
              handler: "reverse_proxy";
              transport: {
                  protocol: "http" | "https";
              };
              upstreams: {
                  dial: string;
              }[];
          }
    )[];
    match: {
        host: string[];
    }[];
}

export interface ICaddyConfig {
    listen: string[];
    routes: ICaddyRoute[];
}
