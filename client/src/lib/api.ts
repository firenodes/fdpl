import axios from "axios";
import { toast } from "react-toastify";

export const api = axios.create({
    baseURL: process.env.API_URI ?? "http://localhost:8080/api"
});

export const handleError = (err: Error) => {
    if (axios.isAxiosError(err)) {
        toast.error(
            err.response?.data?.message
                ? lang.t(err.response?.data?.message)
                : err.message
        );
    } else toast.error(err.message);
};
