# API

docker build -t flowtr/fdpl-api:latest -f ./api/Dockerfile ..
docker tag flowtr/fdpl-api:latest localhost:5000/flowtr/fdpl-api:latest
docker push localhost:5000/flowtr/fdpl-api:latest

# Proxy (caddy)
docker build -t flowtr/fdpl-proxy:latest -f ./caddy/Dockerfile ./caddy
docker tag flowtr/fdpl-proxy:latest localhost:5000/flowtr/fdpl-proxy:latest
docker push localhost:5000/flowtr/fdpl-proxy:latest
