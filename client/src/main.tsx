import { render } from "preact";
import { App } from ".";

const el = document.createElement("div");
document.body.appendChild(el);

render(<App />, el);
