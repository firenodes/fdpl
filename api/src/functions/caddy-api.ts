import { ICaddyConfig } from "@shared/index.js";
import axios, { AxiosInstance } from "axios";

export class CaddyApi {
    protected client: AxiosInstance;

    constructor(readonly baseUrl: string) {
        this.client = axios.create({
            baseURL: baseUrl
        });
    }

    async setConfig(config: ICaddyConfig) {
        const response = await this.client.post(
            "/config/apps/http/servers/fdpl",
            config
        );
        return response;
    }

    async getConfig() {
        const response = await this.client.get<ICaddyConfig>(
            "/config/apps/http/servers/fdpl"
        );
        return response;
    }
}
