import { Logger } from "tslog";

export const logger = new Logger({
    name: "fdpl backend",
    displayFilePath: "hidden",
    displayFunctionName: false,
    printLogMessageInNewLine: true
});
