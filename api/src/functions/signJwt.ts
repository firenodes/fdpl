import jwt from "jsonwebtoken";
import config from "../lib/config.js";
import { logger as mainLogger } from "../lib/logger.js";
import { IUser } from "@shared/index.js";

const logger = mainLogger.getChildLogger({
    name: "fdpl auth"
});

const signJwt = (user: IUser) =>
    new Promise<string>((resolve, reject) => {
        const timeSinceEpoch = new Date().getTime();
        const expirationTime =
            timeSinceEpoch + Number(config.token.expireTime) * 100000;
        const expirationTimeInSeconds = Math.floor(expirationTime / 1000);

        logger.info(`Attempting to sign token for ${user.username}`);

        try {
            jwt.sign(
                {
                    username: user.username,
                    _id: user._id
                },
                config.token.secret,
                {
                    issuer: config.token.issuer,
                    algorithm: "HS256",
                    expiresIn: expirationTimeInSeconds
                },
                (error, token) => {
                    if (error) {
                        reject(error);
                    } else if (token) {
                        resolve(token);
                    }
                }
            );
        } catch (error) {
            logger.prettyError(error as Error);
            reject(error as Error);
        }
    });

export default signJwt;
