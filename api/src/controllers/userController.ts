import { Request, Response, NextFunction } from "express";
import { logger as mainLogger } from "../lib/logger.js";
import mongoose from "mongoose";
import signJwt from "../functions/signJwt.js";
import { argon2id, argon2Verify } from "hash-wasm";
import User from "../models/user.js";
import crypto from "crypto";

const logger = mainLogger.getChildLogger({
    name: "fdpl users"
});

export const register = async (req: Request, res: Response) => {
    const { username, password } = req.body;

    let hash = "";

    try {
        const salt = crypto.randomBytes(16);
        hash = await argon2id({
            password,
            salt, // salt is a buffer containing random bytes
            parallelism: 1,
            iterations: 256,
            memorySize: 512, // use 512KB memory
            hashLength: 32, // output size = 32 bytes
            outputType: "encoded" // return standard encoded string containing parameters needed to verify the key
        });
    } catch (error) {
        return res.status(500).json({
            message: (error as Error).message,
            error
        });
    }
    try {
        const user = new User({
            _id: new mongoose.Types.ObjectId(),
            username,
            password: hash
        });
        await user.save();

        return res.status(201).json({
            user: {
                ...user,
                password: undefined
            }
        });
    } catch (error) {
        return res.status(500).json({
            message: (error as Error).message,
            error
        });
    }
};

export const login = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const { username, password } = req.body;

    try {
        const user = await User.findOne({ username }).exec();
        if (!user) {
            return res.status(404).json({
                message: "User not found"
            });
        }

        try {
            const result = await argon2Verify({
                password,
                hash: user.password
            });

            if (result) {
                try {
                    const token = await signJwt(user);

                    return res.status(200).json({
                        message: "Auth Succesful",
                        token
                    });
                } catch (error) {
                    logger.error("Unable to sign token", error);
                    return res.status(401).json({
                        message: "Unable to sign token",
                        error
                    });
                }
            }
        } catch (error) {
            logger.prettyError(error as Error);
            return res.status(401).json({
                message: (error as Error).message,
                error
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: (error as Error).message,
            error
        });
    }
};

export const getProfile = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const user = await User.findOne({
        _id: res.locals.jwt._id
    })
        .select("-password")
        .exec();

    return res.status(200).json({
        user
    });
};
