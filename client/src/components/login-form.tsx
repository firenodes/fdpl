import { useForm } from "react-hook-form";
import { api, handleError } from "../lib/api";
import { toast } from "react-toastify";
import { redirectPage } from "@nanostores/router";
import { router } from "../stores/router";
import { setUser } from "../stores/user";
import { lang } from "../../../src/util/lang";

export const LoginForm = () => {
    const {
        handleSubmit,
        register,
        formState: { errors }
    } = useForm<{ username: string; password: string }>();

    const onSubmit = (values: { username: string; password: string }) => {
        console.log(values);
        api.post<{ token: string }>("/auth", values, {
            validateStatus: (status) => status === 200
        })
            .then((res) => {
                console.log(res);
                api.get("/user", {
                    headers: { authorization: res.data.token },
                    validateStatus: (status) => status === 200
                })
                    .then((res) => {
                        toast.success(lang.t("loginSuccess"));
                        setUser(res.data);
                        redirectPage(router, "home");
                    })
                    .catch(handleError);
            })
            .catch(handleError);
    };

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <label htmlFor="username">{lang.t("username")}</label>
            <input
                type="text"
                className="border-bottom-input"
                {...register("username", {
                    required: "Required"
                })}
                placeholder="theo"
                name="username"
            />
            <span>{errors.username && errors.username.message}</span>

            <label htmlFor="password">{lang.t("password")}</label>
            <input
                type="password"
                className="border-bottom-input"
                {...register("password", {
                    required: "Required"
                })}
                placeholder="12345"
                name="password"
            />
            <span>{errors.password && errors.password.message}</span>

            <button type="submit">{lang.t("submit")}</button>
        </form>
    );
};
