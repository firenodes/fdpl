import { createRouter } from "@nanostores/router";

// Types for :params in route templates
export interface Routes {
    home: void;
    login: void;
}

export const router = createRouter<Routes>({
    home: "/client/",
    login: "/client/login"
});
